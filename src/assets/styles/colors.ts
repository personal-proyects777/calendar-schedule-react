export const color = {
    primary: '#7030b4',
    secondary: '#4a0d8c',
    disabled: '#bdc3c7',
    hover: '#25248a',
    textPrimary: '#fff',
    textHover: '#fff73f',
    white: '#ffffff',
};