import styled from 'styled-components';
import { v } from '@/assets/styles/variables.ts';
import { color } from '@/assets/styles/colors.ts';

export const SDashboard = styled.div`
    padding: ${v.smSpacing};
    background: ${color.white};
    border-radius: ${v.borderRadius};
    //height: 100%;
`;