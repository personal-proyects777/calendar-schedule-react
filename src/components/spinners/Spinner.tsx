import { Loader, SpinnerWrapper } from '@/assets/styles/components/spinners/spinner.styles.ts';

const Spinner = () => {
    return <SpinnerWrapper>
        <Loader />
    </SpinnerWrapper>;
};

export default Spinner;